import styles from '../styles/projectdemo.module.css'
//import mainimg from '../media/photo.png'
import Image from 'next/image'
import Head from 'next/head'
const Homesection1 = () => {

    return (
        <div className={styles.section1outer1}>
            <div className={styles.subtextdiv1}>
                <div className={styles.left1}>
                    <div className={styles.about1}>
                        <p><span>Deep learning/ML</span></p>
                        <p>The Flask framework was used in the design and development of a website that predicts 
                            the MGMT promoter methylation status in glioblastoma patients based on MRI images</p>
                    </div>
                </div>
                <div className={styles.right1}>
                    <div className={styles.stat1}>
                        <h1>Achieved</h1>
                        <p>89%<br></br>Accuracy</p>
                    </div>
                    <div className={styles.stat1}>
                        <h1>Python</h1>
                        <p>Flask<br></br> CNN/Keras<br></br>Tensorflow</p>
                    </div>
                </div>

            </div>

            {/*<Image src={mainimg} className={styles.mainimg} alt="mainimg" quality={100}/>
            */}
            <div className={styles.maintextdiv1}>
                <p> </p>
                <h1><span>&nbsp;</span></h1>
                
            </div>
            <video src={'Mlv.mp4'} autoPlay loop muted
                className={styles.smokevideo2}
            />
        </div>
    )
}

export default Homesection1
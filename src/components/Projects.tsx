import React, { useRef, useState } from 'react';

import styles from '@/styles/Projects.module.css'
// Import Swiper React components

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';
//import signature from '../media/sign1.png'
import Image from 'next/image'

import '@/styles/ProjectSwiper.css';

// import required modules
import { EffectCoverflow, Pagination } from 'swiper/modules';
const Projects = () => {
    return (
        <div className={styles.projectdiv}>
            <h1>Projects</h1>
            <div className='projectswipercontainer'>
               <Swiper
                    effect={'coverflow'}
                    grabCursor={true}
                    centeredSlides={true}
                    slidesPerView={'auto'}
                    coverflowEffect={{
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: true,
                    }}
                    pagination={true}
                    modules={[EffectCoverflow, Pagination]}
                    className="mySwiper"
                >
                     <SwiperSlide>
                        <div className={styles.projectcard}>
                            <h2>JAVA</h2>
                            <h2>Projects</h2>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.projectcard} onClick={() => window.open('https://gitlab.com/Kavya999-code/atm_interface.git','_blank')}>
                            <h1>ATM Interface</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.projectcard} onClick={() => window.open('https://gitlab.com/Kavya999-code/crm_system.git','_blank')}>
                             <h1>CRM System</h1>
                        </div>
                    </SwiperSlide>
                    
                    <SwiperSlide>
                      <div className={styles.projectcard} onClick={() => window.open('https://gitlab.com/Kavya999-code/exam_seating_arrangement.git','_blank')}>
                      <h1>Exam seating Arrangement System</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.projectcard} onClick={() => window.open('https://gitlab.com/Kavya999-code/criminal_face_detection.git','_blank')}>
                            <h1> Criminal face detection System</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.projectcard} onClick={() => window.open('https://gitlab.com/Kavya999-code/bfit_cognitive_game.git','_blank')}>
                           <h1> bFit Cognitive and Memory Testing Game</h1>
                        </div>
                    </SwiperSlide>
                    
                     <SwiperSlide>
                         <div className={styles.projectcard}  onClick={() => window.open('https://gitlab.com/Kavya999-code/mgmt-prediction.git','_blank')}>
                            <h2>Machine Leanring</h2>
                            <h2>Project</h2>
                        </div>
                    </SwiperSlide>

                    <SwiperSlide>
                        <div className={styles.projectcard}  onClick={() => window.open('https://gitlab.com/Kavya999-code/mgmt-prediction.git','_blank')}>
                            <h3>Brain tumor radiogenomic</h3>
                            <h3>Classification</h3>
                            <h3>using Deep learning/ML</h3>
                           

                        </div>
                    </SwiperSlide>
    
                    
                </Swiper > 
            </div>
        </div>
    )
}

export default Projects
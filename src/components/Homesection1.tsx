import styles from '../styles/Homesection1.module.css'
//import mainimg from '../media/photo.png'
import Image from 'next/image'
import Head from 'next/head'
const Homesection1 = () => {

    return (
        <div className={styles.section1outer}>
            <div className={styles.subtextdiv}>
                <div className={styles.left}>
                    <div className={styles.about}>
                        <p>Hi, I am <span>Kavya M</span></p>
                        <p>I &apos;m Passionate software engineer with strong 
                            academic background, seeking opportunities in innovative tech company for continual growth and creative problem-solving.
                            Collaborate and push boundaries in cutting-edge projects!</p>
                    </div>
                </div>
                <div className={styles.right}>
                    <div className={styles.stat}>
                        <h1>10+</h1>
                        <p>programing<br></br>languages</p>
                    </div>
                    <div className={styles.stat}>
                        <h1>6+</h1>
                        <p>Projects<br></br> experience</p>
                    </div>
                </div>

            </div>

            {/*<Image src={mainimg} className={styles.mainimg} alt="mainimg" quality={100}/>
            */}
            <div className={styles.maintextdiv}>
                <p>I am a </p>
                <h1><span>&nbsp;</span>FULLSTACK   DEVELOPER</h1>
                
            </div>
            <video src={'smoke1.mp4'} autoPlay loop muted
                className={styles.smokevideo}
            />
        </div>
    )
}

export default Homesection1
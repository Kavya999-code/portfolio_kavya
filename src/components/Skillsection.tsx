import React, { useRef, useState } from 'react';

import styles from '@/styles/Projects.module.css'
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';


import '@/styles/ProjectSwiper.css';

// import required modules
import { EffectCoverflow, Pagination } from 'swiper/modules';
const Skillsection = () => {
    return (
        <div className={styles.projectdiv1}>
            <h1>Technical Skills</h1>

            <div className='projectswipercontainer2'>
               <Swiper
                    effect={'coverflow'}
                    grabCursor={true}
                    centeredSlides={true}
                    slidesPerView={'auto'}
                    coverflowEffect={{
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: true,
                    }}
                    pagination={true}
                    modules={[EffectCoverflow, Pagination]}
                    className="mySwiper"



                >
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>Programming Lang:</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>Java,Python,C/C++</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>Frontend lang: </h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h2> HTML,CSS/flex, JS <br></br> ReactJS/NextJS, AngularJS </h2>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>Frameworks</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h2>Hibernet</h2>
                            <h2>Spring Boot</h2>
                            <h2>Node JS</h2>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>Database Lang</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                       <div className={styles.skillcard}>
                            <h1>SQL/PostgreSQL</h1>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                       <div className={styles.skillcard}>
                            <h1>Cloud Technolgies </h1>
                            <h1>Azure</h1>
                        </div>
                    </SwiperSlide>
                     <SwiperSlide>
                        <div className={styles.skillcard}>
                            <h1>containers</h1>
                            <h2>Kubernetes and Docker</h2>
                        </div>
                    </SwiperSlide>
                </Swiper > 
            </div>
        </div>
    )
}

export default Skillsection
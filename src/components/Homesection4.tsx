/* eslint-disable react/no-unescaped-entities */
import styles from '@/styles/Homesection4.module.css';

const Homesection4 = () => {
    return (
        <div className={styles.section4outermost}>
            <div className={styles.left}>
                <p>I thrive on pushing boundaries and driving impactful results.
                     Let &apos;s innovate together for a brighter technological future.</p>
            </div>
            <div className={styles.right}>
                <h1>
                    Ready to<br />
                    change the<br />
                    world
                </h1>
            </div>
        </div>
    )
}

export default Homesection4
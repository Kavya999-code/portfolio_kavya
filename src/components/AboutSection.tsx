import React from 'react'
import styles from '@/styles/AboutSection.module.css'
//import image from 'next/image'
import Img from '@/media/lap.jpg'
import Image from 'next/image'
const AboutSection = () => {
    return (
    
        <div className={styles.about}>
            <div className={styles.textdiv}>
                <h1>About me</h1>
                
               <p>A graduate of computer science engineering, I &apos;m passionate about using technology to address problems in the real world. </p>
               <p>I excel in software engineering with proficiency in Java, Python, C/C++, MySQL/PostgreSQL, Spring frameworks, 
React JS/Next JS, and Agile methodologies, ensuring efficient project delivery with reduced defects.</p>
               <p>My achievements in coding competitions on HackerRank and GeeksforGeeks</p>
                <p>Presented technical papers 
                on Brain Tumor-Radiogenomic Classification, and contributed to AI integration in recruitment processes at IIT Madras. 
               </p>
              </div>
         
          
           <Image src={Img} alt="aboutimg" quality={100} />
       
        
        </div>
        
    )
}

export default AboutSection
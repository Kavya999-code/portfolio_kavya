
"use client"
import React from 'react'; 
import styles from '@/styles/Homesection5.module.css';
import { SocialIcon } from 'react-social-icons'
const Homesection5 = () => {
    return (
        <div className={styles.social}>
            <div className={styles.socialcard}
             onClick={() => window.open('https://drive.google.com/file/d/1-d6PaoHJ-a3OSJRhfv8rBEQWBKDRioIL/view?usp=drive_link','_blank')}
            >
                <SocialIcon url="https://drive.google.com" style={{ height: 40, width: 40 }}/>
                <p>Resume</p>
            </div>
            <div className={styles.socialcard}
             onClick={() => window.open('https://gitlab.com/Kavya999-code','_blank')}
            >
                <SocialIcon url="https://tse1.explicit.bing.net/th?id=OIP.VLG6QH_FqrFykYC2sXawEgHaG1&pid=Api&P=0&h=220" style={{ height: 40, width: 40 }}/>
                <p>Gitlab</p>
            </div>
            <div className={styles.socialcard}
            onClick={() => window.open('https://mail.google.com/mail/u/1/#inbox?compose=GTvVlcSDbtzPlTdtZRCSQQcBZfWPqgBvcLTPTZgpLGCkkCKBCSTVSfZZFkLLPZDFbLKPvTPWVWGzm','_blank')}
            > 
                <SocialIcon url="https://mail.google.com" style={{ height: 40, width: 40 }}/>
                <p>kavyanayaka1999@gmail.com</p>
            </div>
            <div className={styles.socialcard}
            onClick={() => window.open('https://www.linkedin.com/in/kavya-m-857867216/','_blank')}
            >
                <SocialIcon url="https://linkedin.com" style={{ height: 40, width: 40 }}/>
                <p>Kavya M</p>
            </div>
            <div className={styles.socialcard}
            onClick={() => window.open('https://www.hackerrank.com/kavyanayaka1999','_blank')}
            >
                <SocialIcon url="https://hackerrank.com"style={{ height: 40, width: 40 }} />
                <p>HackerRank</p>
            </div>
        </div>
    )
}

export default Homesection5